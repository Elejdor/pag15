#pragma once
#include "Headers.hpp"

using namespace glm;
using namespace std;

class Transform
{
private:
	Transform* _parent;
	vec3 _position;
	quat _rotation;
	vec3 _scale;

	mat4 _modelMatrix;
	
	vector<Transform*> _children;
	bool _isModelMatrixDirty;

public:
	Transform();
	Transform* GetParent() const;
	void SetParent(Transform* newParent);

	vec3 GetPosition() const;
	void SetPosition(const vec3& newPosition);

	quat GetRotation() const;
	void SetRotation(const quat& newRotation);

	vec3 GetLocalPosition() const;
	void SetLocalPosition(const vec3& newLocalPosition);

	quat GetLocalRotation() const;
	void SetLocalRotation(const quat& newLocalRotation);

	mat4 GetModelMatrix();

	void Rotate(const vec3& axis, const float& angle);
	void Translate(const vec3& offset);

	void AddChild(Transform* child);
	void RemoveChild(Transform* child);

	void UpdateModelMatrix();

	void UpdateOrientation(const quat& oldParentRot, const vec3& oldParentPos);
	
	vec3 right;
	vec3 up;
	vec3 forward;



	~Transform();
};


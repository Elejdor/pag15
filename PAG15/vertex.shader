#version 330 core
layout(location = 0) in vec3 s_vPosition;
layout(location = 1) in vec2 texCoord;
layout(location = 2) in vec3 normalIN;

uniform mat4 MVP;

out vec4 normalOUT;
out vec2 UV;

void main()
{
	vec4 v = vec4(s_vPosition, 1.0f);
	normalOUT = MVP * vec4(normalIN, 0.0f);
	gl_Position = MVP * v;
	UV = texCoord;
}
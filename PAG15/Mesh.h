#pragma once
#include "Headers.hpp"
#include <Windows.h>

#define GLFW_INCLUDE_GLU
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "Globals.h"
#include "Transform.h"
#pragma warning (disable : 4996)

class Mesh
{
private:
	struct VertexEncapsulator
	{
		vec3 position;
		vec3 normal;
		vec2 uv;
	};

	vector<GLfloat> vertices;
	vector<GLfloat> uvs;
	vector<GLfloat> normals;
	vector<GLuint> indices;
	vec3 id;

public:

	Transform transform;
	
	vec3 GetId()
	{
		return id;
	}

	void SetId(int id)
	{
		this->id.r = (float)(id % 256) / 255;
		id >>= 8;
		this->id.g = (float)(id % 256) / 255;
	}

	GLfloat* verticesData;
	GLfloat* uvsData;
	GLfloat* normalsData;
	GLuint* indicesData;

	Mesh();
	~Mesh();

	GLuint vertexBuffer;
	GLuint indexBuffer;
	GLuint normalBuffer;
	GLuint uvBuffer;
	
	GLuint textureBuffer;

	int indicesSize;
	int verticesSize;
	int normalsSize;

	//void Draw() const;
	void SetVertices(GLfloat vertices[], int size);
	void SetIndices(GLuint indices[], int size);
	void SetNormals(GLfloat normals[], int size);
	void Mesh::SetUVs(GLfloat uvs[], int size);

	void Load(const string& path);
};


#version 330 core
layout(location = 0) in vec3 s_vPosition;
layout(location = 1) in vec2 texCoord;

out vec2 UV;


void main(){
	gl_Position = vec4(s_vPosition, 1);
	UV = texCoord;
}
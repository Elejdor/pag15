#pragma once
#include "Headers.hpp"
#include <GL\glew.h>

#include <GLFW\glfw3.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

extern GLFWwindow* mainWindow;
extern GLuint LoadTexture(const char* path);

extern char* ReadFileFopen(const char* fileName);
extern string ReadFileStream(const char* fileName);
extern bool KeyDown(int key);

extern int CountIDFromColorbv(unsigned char color[3]);
extern void CompileShader(GLuint shaderID, const char* shaderSourcePtr);
extern GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);
#version 330 core
layout(location = 0) in vec3 s_vPosition;

uniform mat4 MVP;

void main()
{
	vec4 v = vec4(s_vPosition, 1.0f);
	gl_Position = MVP * v;
}
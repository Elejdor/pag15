#include "mesh.h"

Mesh::Mesh()
{

}


Mesh::~Mesh()
{
	glDeleteBuffers(1, &vertexBuffer);
	glDeleteBuffers(1, &indexBuffer);
}

void Mesh::SetIndices(GLuint indices[], int size)
{
	glGenBuffers(1, &indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, indices, GL_STATIC_DRAW);
	indicesSize = size;
}

void Mesh::SetVertices(GLfloat vertices[], int size)
{
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
	verticesSize = size;
}

void Mesh::SetNormals(GLfloat normals[], int size)
{
	glGenBuffers(1, &normalBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
	glBufferData(GL_ARRAY_BUFFER, size, normals, GL_STATIC_DRAW);
	normalsSize = size;
}

void Mesh::SetUVs(GLfloat uvs[], int size)
{
	glGenBuffers(1, &uvBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
	glBufferData(GL_ARRAY_BUFFER, size, uvs, GL_STATIC_DRAW);
}

void Mesh::Load(const string& path)
{
	std::vector< GLuint > vertexIndices, uvIndices, normalIndices;
	std::vector< glm::vec3 > temp_vertices;
	std::vector< glm::vec2 > temp_uvs;
	std::vector< glm::vec3 > temp_normals;

	FILE * file = fopen(path.c_str(), "r");
	if (file == NULL){
		printf("Impossible to open the file !\n");
		return ;
	}

	while (1){

		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		if (strcmp(lineHeader, "v") == 0){
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0){
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0){
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0){
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9){
				printf("File can't be read by our simple parser : ( Try exporting with other options\n");
				return ;
			}

			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
	}


	int countVerts = vertexIndices.size();
	bool exists = false;
	vector<VertexEncapsulator> vhs;
	int indexing = 0;

	for (int i = 0; i < countVerts; i++)
	{
		indexing = vhs.size();
		exists = false;

		for (int j = 0; j < vhs.size(); j++)
		{
			if (vhs[j].position == temp_vertices[vertexIndices[i]-1] &&
				vhs[j].normal == temp_normals[normalIndices[i]-1] &&
				vhs[j].uv == temp_uvs[uvIndices[i]-1])
			{
				exists = true;
				indexing = j;
				break;
			}
		}
		
		if (!exists)
		{
			VertexEncapsulator vh;
			vh.position = temp_vertices[vertexIndices[i] - 1];
			vh.normal = temp_normals[normalIndices[i] - 1];
			vh.uv = temp_uvs[uvIndices[i] - 1];

			vhs.push_back(vh);
			indices.push_back(indexing);
		}
		else
		{
			indices.push_back(indexing);
			
		}
	}

	for (int i = 0; i < vhs.size(); i++)
	{
		float* pos = value_ptr(vhs[i].position);
		float* normal = value_ptr(vhs[i].normal);
		float* uv = value_ptr(vhs[i].uv);

		vertices.push_back(*(pos));
		vertices.push_back(*(pos+1));
		vertices.push_back(*(pos+2));

		normals.push_back(*(normal));
		normals.push_back(*(normal + 1));
		normals.push_back(*(normal + 2));
		
		uvs.push_back(*(uv));
		uvs.push_back(*(uv+1));		
	}

	
	vector<GLuint> invIndices;
	for (int i = indices.size() - 1; i >= 0; --i)
	{
		invIndices.push_back(indices[i]);
	}

	indices = invIndices;

	SetVertices(vertices.data(), vertices.size() * sizeof(GLfloat));
	SetIndices(indices.data(), indices.size() * sizeof(GLuint));
	SetNormals(normals.data(), normals.size() * sizeof(GLfloat));
	SetUVs(uvs.data(), uvs.size() * sizeof(GLfloat));
}
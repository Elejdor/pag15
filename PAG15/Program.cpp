#include "Program.h"

Program::Program()
{
	cam = new Camera(vec3(0, 0.2f, -0.8));
	float ar = (float)SCREEN_WIDTH / SCREEN_HEIGHT;
	cam->CreatePerspectiveProjection(0.87f, 1.33f, 0.1f, 100.f);
	//cam->CreateOrthogonalProjection(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT, 0.01f, 1000.0f);

	//Create framer buffer	
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	//Initialize render to target
	glGenTextures(1, &sceneTexture);
	glBindTexture(GL_TEXTURE_2D, sceneTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//Depth buffer
	GLuint depthBuffer;
	glGenRenderbuffers(1, &depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, SCREEN_WIDTH, SCREEN_HEIGHT);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
	
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, sceneTexture, 0);
	
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);
	
	CreateQuad();
}

void Program::Load()
{
	standardShader = LoadShaders("vertex.shader", "fragment.shader");
	quadShader = LoadShaders("quadVertex.shader", "quadFragment.shader");
	selectionShader = LoadShaders("selectionVertex.shader", "selectionFragment.shader");

	Mesh* tmpMesh;

	tmpMesh = new Mesh();
	tmpMesh->Load("box.obj");
	tmpMesh->textureBuffer = LoadTexture("cube_tex.bmp");
	tmpMesh->SetId(1);
	meshes.push_back(tmpMesh);

	tmpMesh = new Mesh();
	tmpMesh->Load("box.obj");
	tmpMesh->textureBuffer = LoadTexture("cube_tex.bmp");
	tmpMesh->SetId(2);
	tmpMesh->transform.SetPosition(vec3(2.0f, 0, 0));
	tmpMesh->transform.SetParent(&meshes[0]->transform);
	meshes.push_back(tmpMesh);

	//tmpMesh->transform.SetParent(&testMesh->transform);

	//float f = testMesh->GetId();
	//char* bits = reinterpret_cast<char*>(&f);
	//std::cout << f << endl;
	//for (std::size_t n = 0; n < 4; ++n)
	//	std::cout << std::bitset<8>(bits[n]);	

}


Program::~Program()
{
	delete(cam);
}

#pragma region Logic Code

void Program::Update()
{
	HandleInput();
}

void Program::HandleInput()
{
	//Selection with mouse click
	double xpos, ypos;
	glfwGetCursorPos(mainWindow, &xpos, &ypos);
	int mouseState = glfwGetMouseButton(mainWindow, 0);
	if (mouseState == GLFW_PRESS)
	{
		printf("Mouse pos: %f, %f\n", xpos, ypos);

		unsigned char pixel[3];
		
		glBindFramebuffer(GL_READ_FRAMEBUFFER, frameBuffer);
		glReadBuffer(GL_COLOR_ATTACHMENT0);
		glReadPixels((int)xpos, SCREEN_HEIGHT - (int)ypos, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);

		selectedId = CountIDFromColorbv(pixel);
		
		printf("Selected object (%d)\n", selectedId);
	}
		

	//Move selected objects
	if (selectedId > 0)
	{
		Mesh* selectedMesh = meshes[selectedId - 1];
		Transform* trans = &selectedMesh->transform;
		mat4 transformation = trans->GetModelMatrix(); //use it for forward movement
		float y = 0, p = 0, r = 0;
		float rotationSpeed = 0.1f;

		float translationSpeed = 0.01f;
		vec3 direction;

		//relative translation
		if (KeyDown(GLFW_KEY_UP))
		{			
			direction += trans->forward;
		}
		if (KeyDown(GLFW_KEY_DOWN))
		{
			direction += -trans->forward;
		}
		if (KeyDown(GLFW_KEY_RIGHT))
		{
			direction += trans->right;
		}
		if (KeyDown(GLFW_KEY_LEFT))
		{
			direction += -trans->right;
		}
		if (KeyDown(GLFW_KEY_EQUAL)) // +
		{
			direction += trans->up;
		}
		if (KeyDown(GLFW_KEY_MINUS)) // -
		{
			direction += -trans->up;
		}

		//world translation
		if (KeyDown(GLFW_KEY_W))
		{
			direction += vec3(0, 0, 1);
		}
		if (KeyDown(GLFW_KEY_S))
		{
			direction += -vec3(0, 0, 1);
		}
		if (KeyDown(GLFW_KEY_D))
		{
			direction += vec3(1, 0, 0);
		}
		if (KeyDown(GLFW_KEY_A))
		{
			direction += -vec3(1, 0, 0);
		}
		if (KeyDown(GLFW_KEY_Q))
		{
			direction += vec3(0, 1, 0);
		}
		if (KeyDown(GLFW_KEY_E))
		{
			direction += -vec3(0, 1, 0);
		}

		//Rotations
		//Pitch
		if (KeyDown(GLFW_KEY_HOME))
		{
			p += rotationSpeed;
		}
		if (KeyDown(GLFW_KEY_END))
		{
			p += -rotationSpeed;
		}
		//Yaw
		if (KeyDown(GLFW_KEY_PAGE_DOWN))
		{
			y += rotationSpeed;
		}
		if (KeyDown(GLFW_KEY_DELETE))
		{
			y += -rotationSpeed;
		}
		//Roll
		if (KeyDown(GLFW_KEY_PAGE_UP))
		{
			r += rotationSpeed;
		}
		if (KeyDown(GLFW_KEY_INSERT))
		{
			r += -rotationSpeed;
		}

		trans->Rotate(vec3(1, 0, 0), p);
		trans->Rotate(vec3(0, 1, 0), y);
		trans->Rotate(vec3(0, 0, 1), r);

		normalize(direction);
		trans->Translate(direction * translationSpeed);
	}
}

#pragma endregion

#pragma region Rendering Code
void Program::CreateQuad()
{
	GLuint quadVertexArrayID;
	glGenVertexArrays(1, &quadVertexArrayID);
	glBindVertexArray(quadVertexArrayID);

	GLfloat g_quad_vertex_buffer_data[] = {
		1.0f, 1.0f, 0.0f, //1, 1,
		-1.0f, 1.0f, 0.0f, //0, 1,
		1.0f, -1.0f, 0.0f, //1, 0,
		-1.0f, -1.0f, 0.0f//, 0, 0
	};

	GLfloat g_quad_uvs_buffer_data[] =
	{
		1, 1,
		0, 1,
		1, 0,
		0, 0
	};	

	GLuint g_quad_index_buffer_data[] =
	{
		3, 2, 1, 0
	};
	
	quad.SetVertices(g_quad_vertex_buffer_data, sizeof(g_quad_vertex_buffer_data));
	quad.SetUVs(g_quad_uvs_buffer_data, sizeof(g_quad_uvs_buffer_data));
	quad.SetIndices(g_quad_index_buffer_data, sizeof(g_quad_index_buffer_data));	
}



void Program::DrawMesh(Mesh* mesh)
{
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, mesh->uvBuffer);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, mesh->normalBuffer);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureBuffer);
	glUniform1i(glGetUniformLocation(standardShader, "diffuse"), 0);

	mat4 MVP = (*cam->ViewProjection) * scale(mat4(), vec3(0.1f, 0.1f, 0.1f)) * mesh->transform.GetModelMatrix();
	glUniformMatrix4fv(glGetUniformLocation(standardShader, "MVP"), 1, GL_FALSE, value_ptr(MVP));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
	glDrawElements(GL_TRIANGLES, mesh->indicesSize, GL_UNSIGNED_INT, NULL);


}

void Program::DrawId(Mesh* mesh)
{
	mat4 MVP = (*cam->ViewProjection) * scale(mat4(), vec3(0.1f, 0.1f, 0.1f)) * mesh->transform.GetModelMatrix();

	//Draw selection IDs
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glUniformMatrix4fv(glGetUniformLocation(standardShader, "MVP"), 1, GL_FALSE, value_ptr(MVP));

	vec3 idVal = vec3(mesh->GetId().r, mesh->GetId().g, 0);
	glUniform3f(glGetUniformLocation(selectionShader, "id"), idVal.r, idVal.g, idVal.b);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
	glDrawElements(GL_TRIANGLES, mesh->indicesSize, GL_UNSIGNED_INT, NULL);
}

void Program::Draw()
{

	glClearColor(0, 0, 0, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (auto &it : meshes)
	{
		glUseProgram(standardShader);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		DrawMesh(it);	

		glUseProgram(selectionShader);
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

		DrawId(it);
	}

	for (auto &it : meshes)
	{
		
	}

	//DrawMesh(testMesh);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//vertices
	if (debugId)
	{
		//Draw fullscreen quad-----------
		glUseProgram(quadShader);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		//texture
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, sceneTexture);
		glUniform1i(glGetUniformLocation(quadShader, "sceneTex"), 0);

		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, quad.vertexBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, quad.uvBuffer);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quad.indexBuffer);
		glDrawElements(GL_TRIANGLE_STRIP, quad.indicesSize / sizeof(GLuint), GL_UNSIGNED_INT, NULL);
	}


}
#pragma endregion

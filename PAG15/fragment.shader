#version 330 core
layout(location = 0) out vec3 color;

in vec4 normalOUT;
uniform sampler2D diffuse;
in vec2 UV;

void main(){
	vec3 light = normalize(vec3(0, 2, -1));
	float NdotL = dot(vec3(normalOUT), light);
    color = texture( diffuse, UV).xyz; //NdotL * vec3(1.0f, 1.0f, 1.0f) + 0.1f;
}
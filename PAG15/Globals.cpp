#include "Globals.h"
//#pragma warning (disable : 4996)

GLFWwindow* mainWindow = nullptr;

int CountIDFromColorbv(unsigned char color[3])
{
	return color[0] + color[1] * 256 + color[2] * 256 * 65536;
}

GLuint LoadTexture(const char* path)
{
	unsigned char header[54];
	uint dataPos;
	uint width, height;
	uint imageSize;

	FILE* file = fopen(path, "rb");
	if (!file)
	{
		printf("could'n open file: %s", path);
		return 0;
	}

	if (fread(header, 1, 54, file) != 54)
	{
		printf("Incorrect BMP file: %s", path);
		return 0;
	}

	if (header[0] != 'B' || header[1] != 'M')
	{
		printf("Incorrect BMP header: %s", path);
		return 0;
	}

	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);
	unsigned char* data;

	//assumption for missformatted file
	if (imageSize == 0)    imageSize = width*height * 3;
	if (dataPos == 0)      dataPos = 54;

	// Create a buffer
	data = new unsigned char[imageSize];

	// Read the actual data from the file into the buffer
	fread(data, 1, imageSize, file);

	//Everything is in memory now, the file can be closed
	fclose(file);

	GLuint texID;
	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	return texID;
}

char* ReadFileFopen(const char* fileName)
{
	FILE* fp = fopen(fileName, "r");
	fseek(fp, 0, SEEK_END);
	long file_length = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	char* contents = new char[file_length + 1];

	for (int i = 0; i < file_length + 1; i++)
	{
		contents[i] = 0;
	}

	fread(contents, 1, file_length, fp);
	contents[file_length + 1] = '\0';
	fclose(fp);

	return contents;
}

string ReadFileStream(const char* fileName)
{
	string result;
	ifstream inputStream(fileName, std::ios::in);
	if (inputStream.is_open())
	{
		string line = "";
		while (getline(inputStream, line))
			result += "\n" + line;
		inputStream.close();
	}

	return result;
}


bool KeyDown(int key)
{
	return (glfwGetKey(mainWindow, key)) == GLFW_PRESS;
}

void CompileShader(GLuint shaderID, const char* shaderSourcePtr)
{
	GLint result = GL_FALSE;
	int infoLogLength;

	//Compile shader
	glShaderSource(shaderID, 1, &shaderSourcePtr, NULL);
	glCompileShader(shaderID);

	//Check shader
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
	vector<char> shaderErrorMessage(infoLogLength);
	glGetShaderInfoLog(shaderID, infoLogLength, NULL, &shaderErrorMessage[0]);
	printf("%s\n", &shaderErrorMessage[0]);
}



GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path)
{
	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode = ReadFileStream(vertex_file_path);

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode = ReadFileStream(fragment_file_path);

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	CompileShader(VertexShaderID, VertexSourcePointer);

	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path);
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	CompileShader(FragmentShaderID, FragmentSourcePointer);

	// Link the program
	fprintf(stdout, "Linking program\n");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> ProgramErrorMessage(std::max(InfoLogLength, int(1)));
	glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
	fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}
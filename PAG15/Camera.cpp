#include "Camera.h"


Camera::Camera(vec3 position) : position(position)
{
	projection = new mat4;
	view = new mat4;
	ViewProjection = new mat4;
}


Camera::~Camera()
{
}

void Camera::UpdateVP()
{
	*view = translate(mat4(), position);
	*ViewProjection = ((*projection) * (*view));
}

void Camera::CreatePerspectiveProjection(float fov, float ar, float zNear, float zFar)
{
	*projection = perspective(fov, ar, zNear, zFar);
	UpdateVP();
}

void Camera::CreateOrthogonalProjection(float left, float right, float top, float bottom, float zNear, float zFar)
{
	projection = &ortho(left, right, top, bottom);
	UpdateVP();
}
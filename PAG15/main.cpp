﻿#include "Program.h"
#include "Headers.hpp"

#define GLFW_INCLUDE_GLU
#include <GLFW\glfw3.h>


int main(void)
{

	/* Initialize the library */
	if (!glfwInit())
		return -1;	
	
	/* Create a windowed mode window and its OpenGL context */
	mainWindow = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Hello World", NULL, NULL);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	if (!mainWindow)
	{
		glfwTerminate();
		return -1;
	}
	

	glewExperimental = true;

	/* Make the window's context current */
	glfwMakeContextCurrent(mainWindow);

	if (glewInit() != GLEW_OK)
		return -1;
	
	Program* myProgram = new Program();
	myProgram->Load();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);

	//glFrontFace(GL_CW);
	/*glCullFace(GL_FRONT);*/
	/*glEnable(GL_CULL_FACE);*/

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(mainWindow))
	{
		int width = SCREEN_WIDTH, height = SCREEN_HEIGHT;
		float ratio = (float)width / height;

		/* Render here */
		glViewport(0, 0, width, height);
		//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		myProgram->Update();
		myProgram->Draw();		

		/* Swap front and back buffers */
		glfwSwapBuffers(mainWindow);

		/* Poll for and process events */
		glfwPollEvents();
	}

	delete(myProgram);

	glfwTerminate();
	return 0;
}


#include "Transform.h"


Transform::Transform()
{
	_parent = nullptr;
	_position = vec3(0.f, 0.f, 0.f);
	_rotation = quat();
	_scale = vec3(1.f, 1.f, 1.f);

	_modelMatrix = mat4();

	_isModelMatrixDirty = true;
}

Transform::~Transform()
{
}

Transform* Transform::GetParent() const
{
	return _parent;
}

void Transform::SetParent(Transform* newParent)
{
	if (_parent)
		_parent->RemoveChild(this);

	if (newParent)
	{
		_parent = newParent;
		_parent->AddChild(this);
	}
}

vec3 Transform::GetPosition() const
{
	return _position;
}
void Transform::SetPosition(const vec3& newPosition)
{
	vec3 childOffset;
	int childCount = _children.size();
	for (int i = 0; i < childCount; ++i)
	{
		childOffset = _children[i]->GetPosition() - _position;
		_children[i]->SetPosition(newPosition + childOffset);
	}

	_position = newPosition;
	_isModelMatrixDirty = true;
}

quat Transform::GetRotation() const
{
	return _rotation;
}
void Transform::SetRotation(const quat& newRotation)
{
	quat myOldRot = _rotation;
	_rotation = newRotation;
	_rotation = normalize(_rotation);

	_isModelMatrixDirty = true;
	int childCount = _children.size();
	for (int i = 0; i < childCount; ++i)
	{
		_children[i]->UpdateOrientation(myOldRot, _position);
	}
}

vec3 Transform::GetLocalPosition() const
{
	if (_parent)
	{
		vec3 local = _position - _parent->GetPosition();
		quat invParentRotation = inverse(_parent->GetRotation());
		return invParentRotation * local;
	}

	return _position;
}
void Transform::SetLocalPosition(const vec3& newLocalPosition)
{
	if (!_parent)
	{
		SetPosition(newLocalPosition);
		return;
	}

	float offsetLength = length(newLocalPosition);
	if (offsetLength > 0.f)
	{
		vec3 localPosition = _parent->GetRotation() * newLocalPosition;
		localPosition = normalize(localPosition);
		localPosition *= offsetLength;

		SetPosition(_parent->GetPosition() + localPosition);
	}
	else
		SetPosition(_parent->GetPosition());
}

quat Transform::GetLocalRotation() const
{
	if (_parent)
	{
		quat invParentRotation = inverse(_parent->GetRotation());
		return invParentRotation * _rotation;
	}

	return _rotation;
}
void Transform::SetLocalRotation(const quat& newLocalRotation)
{
	if (!_parent)
	{
		SetRotation(newLocalRotation);
		return;
	}

	SetRotation(_parent->GetRotation() * newLocalRotation);
}
mat4 Transform::GetModelMatrix()
{
	if (_isModelMatrixDirty)
		UpdateModelMatrix();

	return _modelMatrix;
}

void Transform::Rotate(const vec3& axis, const float& angle)
{
	quat newRotation = angleAxis(angle, axis);
	SetRotation(_rotation * newRotation);
}
void Transform::Translate(const vec3& offset)
{
	SetLocalPosition(GetLocalPosition() + offset);
}

void Transform::AddChild(Transform* child)
{
	if (!(find(_children.begin(), _children.end(), child) != _children.end()))
		_children.push_back(child);

	child->_parent = this;
}
void Transform::RemoveChild(Transform* child)
{
	auto i = find(_children.begin(), _children.end(), child);
	if (i != _children.end())
		_children.erase(i);
}

void Transform::UpdateModelMatrix()
{
	_modelMatrix = mat4();

	_modelMatrix *= translate(mat4(), _position);
	_modelMatrix *= mat4_cast(_rotation);

	//update vectors
	right.x = _modelMatrix[0][0];
	right.y = _modelMatrix[0][1];
	right.z = _modelMatrix[0][2];
	normalize(right);

	up.x = _modelMatrix[1][0];
	up.y = _modelMatrix[1][1];
	up.z = _modelMatrix[1][2];
	normalize(up);

	forward.x = _modelMatrix[2][0];
	forward.y = _modelMatrix[2][1];
	forward.z = _modelMatrix[2][2];
	normalize(forward);
	
	_isModelMatrixDirty = false;
}
void Transform::UpdateOrientation(const quat& oldParentRot, const vec3& oldParentPos)
{
	quat myOldRot = _rotation;
	vec3 myOldPos = _position;

	quat invOldParentRot = inverse(oldParentRot);

	if (_position != oldParentPos)
	{
		float distanceToParent = length(_position - oldParentPos);

		vec3 local = invOldParentRot * (_position - oldParentPos);
		local = normalize(local);
		local *= distanceToParent;

		vec3 newLocal = _parent->GetRotation() * local;
		newLocal = normalize(newLocal);
		newLocal *= distanceToParent;

		_position = _parent->GetPosition() + newLocal;
	}

	_rotation = _parent->GetRotation() * invOldParentRot * _rotation;

	_isModelMatrixDirty = true;
	int childCount = _children.size();
	for (int i = 0; i < childCount; ++i)
		_children[i]->UpdateOrientation(myOldRot, myOldPos);
}
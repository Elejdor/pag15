#pragma once
#include "Headers.hpp"
#include "Mesh.h"
#include "Camera.h"
#include <bitset>
#include <GL\glew.h>
#include <GLFW\glfw3.h>

class Program
{
private:
	//GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);

	GLuint standardShader;
	GLuint quadShader;
	GLuint selectionShader;

	Camera* cam;

	const bool debugId = false;
	int selectedId;

	vector<Mesh*> meshes;

	//Redner to texture
	GLuint sceneTexture;
	GLuint frameBuffer;
	Mesh quad;

	void CreateQuad();
	void DrawMesh(Mesh* mesh);
	void DrawId(Mesh* mesh);
	void HandleInput();

public:
	Program();

	void Load();
	void Draw();
	void Update();

	~Program();
};


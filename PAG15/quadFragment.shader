#version 330 core
 
in vec2 UV;
 
out vec3 color;

uniform sampler2D sceneTexture;
 
void main(){

    color = texture( sceneTexture, UV).xyz;
    //color = vec3(1, 0, 0);
}
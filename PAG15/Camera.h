#pragma once
#include "Headers.hpp"
#include "Transform.h"

class Camera
{
private:
	vec3 position;

public:
	Camera(vec3 position);
	~Camera();

	mat4* projection;
	mat4* view;
	mat4* ViewProjection;

	void UpdateVP();
	void CreatePerspectiveProjection(float fov, float ar, float zNear, float zFar);
	void CreateOrthogonalProjection(float left, float right, float top, float bottom, float zNear, float zFar);


};


#version 330 core

layout(location = 0) out vec3 color;
uniform vec3 id;

void main(){
    color = vec3(id.r, id.g, id.b);
}